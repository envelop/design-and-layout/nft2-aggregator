
var tags = document.querySelectorAll(".filter-tag input");
var cards = document.getElementsByClassName("col-card ");
var prev = null;


// удалить

var cardsDemo = document.getElementsByClassName("sec-cards__item");
var modalDemo = document.getElementsByClassName("modal");
var modalCloseDemo = document.getElementsByClassName("modal__close");

for (var i = 0; i < cardsDemo.length; i++) {
    cardsDemo[i].addEventListener('click', function () {
        modalDemo[0].style.display = "block";
    });
}
modalCloseDemo[0].addEventListener('click', function () {
    modalDemo[0].style.display = "none";
});



// tag switch
for (var i = 0; i < tags.length; i++) {

    tags[i].addEventListener('change', function () {
        // (prev) ? console.log(prev.value): null;
        if (this !== prev) {
            prev = this;
        }
        filterCardsByTag(prev.value)

    });
}


function filterCardsByTag(tag) {
    if (tag == "all") {
        for (var i = 0; i < cards.length; i++) {
            cards[i].style.display = "block";
        }
        return;
    }

    for (var i = 0; i < cards.length; i++) {
        if (cards[i].classList.contains(tag)) {
            cards[i].style.display = "block";
        } else {
            cards[i].style.display = "none";
        }
    }
}

// News swiper
var swiperNews = new Swiper('.swiper-news', {
    // Optional parameters
    loop: true,
    spaceBetween: 0,
    crossFade: true,
    effect: 'fade',
    autoplay: {
        delay: 2000,
    },
    speed: 700,
    disableOnInteraction: false,
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
    },
    navigation: {
        nextEl: '.swiper-news-button-next',
        prevEl: '.swiper-news-button-prev',
    },
});


// Digests accordion
var accordion = new Accordion('.accordion-container', {
    hideAll: false,
    showAll: false,
    showFirst: true,
    panelId: 0
});